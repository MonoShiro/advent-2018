import sys
from collections import Counter
from itertools import combinations

i_ = sys.stdin.read().strip()
data = [i for i in i_.split('\n')]

twos = threes = 0

for d in data:
	vals = Counter(d).values()
	if 2 in vals: twos += 1
	if 3 in vals: threes += 1

print('part1:', threes * twos)

for a, b in combinations(data, 2):
	result = ''.join([i for i, j in zip(a, b) if i == j])
	if len(result) == len(a) - 1:
		print('part2:', result)
		exit()

