import sys
import re

RE_CLAIM = re.compile(r'#(\d+)\s@\s(\d+),(\d+):\s(\d+)x(\d+)')
i_ = [i for i in sys.stdin.read().strip().split('\n')]
FABRIC = [[set() for j in range(1000)] for i in range(1000)]
a = set()
bad = set()
for i in i_:
	m = RE_CLAIM.match(i)
	a.add(m.group(1))
	o_l = int(m.group(2))
	o_t = int(m.group(3))
	w = int(m.group(4))
	h = int(m.group(5))
	for j in range(h):
		for k in range(w):
			FABRIC[j + o_t][k + o_l].add(m.group(1))

overlap = 0
for row in FABRIC:
	for col in row:
		if len(col) > 1:
			overlap += 1
			bad |= col

print('part1:', overlap)
print('part2:', ''.join(a - bad))

