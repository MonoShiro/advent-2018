import sys
num = set()
input_ = sys.stdin.read().strip()
sum_ = 0
data = [int(i) for i in input_.split('\n')]
repeat = None
while 1:
	for i in data:
		num.add(sum_)
		sum_ += i
		if sum_ in num:
			print('part2:', sum_)
			exit()	
	if repeat is None:
		print('part1:', sum_)
		repeat = 1
